//this function will ensure to run all the function after the DOM loaded
jQuery(document).ready(function ($) {

    //this function will add a box at the first
    $("#add_first").on('click', function (e) {
            var child_div = $("<div class='child_box'><button class=\"triggerButton btn btn-warning btn-sm\">REMOVE</button></div>");
            $("#parent_box").prepend(child_div);
    });

    //this function will add a box at the end
    $("#add_last").on('click', function (e) {
        var child_div = $("<div class='child_box'><button class=\"triggerButton btn btn-warning btn-sm\">REMOVE</button></div>");
        $("#parent_box").append(child_div);
    });

    //this function will remove a box from the beginning
    $("#delete_first").on('click', function (e) {
        $(".child_box:first").remove();
    });

    //this function will remove a box from the end
    $("#delete_last").on('click', function (e) {
        $(".child_box:last").remove();
    });


    //this function will add color to add the existing boxes
    $("#add_color").on('click', function (e) {
        $(".child_box").addClass("red_child_box");
    });

    //this function will remove color to add the existing boxes
    $("#remove_color").on('click', function (e) {
        $(".child_box").removeClass("red_child_box");
    });

    //this function will delete all the boxes
    $("#delete_all").on('click', function (e) {
        $("#parent_box").empty();
    });

    //this function will delete box on button click
    // $(".triggerButton").click(function(){
    //     $(this).parent().remove();
    // });

    $("#parent_box").on("click", '.triggerButton', function (e) {
        $(this).parent().remove();
    });

});
