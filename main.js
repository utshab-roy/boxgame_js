//this function add a new box beside the existing box
function addOperation(){
    var child_class = document.createElement("div");
    child_class.className = "child_box";
    var parent_class = document.getElementById("parent_box");
    parent_class.appendChild(child_class);
}

//this function remove the last box from the existing
function deleteOperation() {
    var parent_class = document.getElementById("parent_box");
    var last_class = parent_class.lastElementChild; //getting the last child element
    parent_class.removeChild(last_class);
}

//this function change the color of all the existing box to red
function changeColor() {
    var total_child_box = document.getElementById("parent_box").getElementsByClassName("child_box").length;
    for (var i = 0; i < total_child_box; i++){
        var box_item = document.getElementById("parent_box").getElementsByClassName("child_box")[i];
        box_item.classList.add("red_child_box");
    }
}

//this function will remove the color from the box
function removeColor() {
    var total_child_box = document.getElementById("parent_box").getElementsByClassName("child_box").length;
    for (var i = 0; i < total_child_box; i++){
        var box_item = document.getElementById("parent_box").getElementsByClassName("child_box")[i];
        box_item.classList.remove("red_child_box");
    }
}

/****************/
//this function add a box at the first of the index
function addFirstIndex(){
    var child_div = document.createElement("div");          //creating div element
    child_div.className = "child_box";                      //giving class name to that div

    var button = document.createElement("BUTTON");          // Create a <button> element
    var text = document.createTextNode("REMOVE");           // Create a text node
    button.appendChild(text);                               //inserting text to the button

    button.className = 'triggerBtn btn btn-warning btn-sm'; //giving class name to the button

    child_div.appendChild(button);                          //generating the button

    var parent_class = document.getElementById("parent_box");

    parent_class.insertBefore(child_div, parent_class.childNodes[0]);

    optionForBoxDelete();
}

//this function add a box at the last of the index
function addLastIndex(){
    var child_div = document.createElement("div");
    child_div.className = "child_box";

    var button = document.createElement("BUTTON");        // Create a <button> element
    var text = document.createTextNode("REMOVE");       // Create a text node
    button.appendChild(text);

    button.className = 'triggerBtn btn btn-warning btn-sm';

    child_div.appendChild(button);

    var parent_class = document.getElementById("parent_box");
    parent_class.appendChild(child_div);
    optionForBoxDelete();
}

//this function remove the first box from the existing
function deleteFirstIndex() {
    var parent_class = document.getElementById("parent_box");
    var first_class = parent_class.firstElementChild; //getting the first child element
    parent_class.removeChild(first_class);
}

//this function remove the last box from the existing
function deleteLastIndex() {
    var parent_class = document.getElementById("parent_box");
    var last_class = parent_class.lastElementChild; //getting the last child element
    parent_class.removeChild(last_class);
}


//this function add the ability to delete each box by clicking on the remove button
function optionForBoxDelete() {
    var total_child_boxes = document.getElementById("parent_box").getElementsByClassName("child_box").length;
    for (var i = 0; i < total_child_boxes; i++) {
        var button = document.getElementById("parent_box").getElementsByClassName("child_box")[i].getElementsByClassName('triggerBtn');
        if (button[0] !== undefined) {
            button[0].addEventListener("click", function (event) {
                console.log('clicked');
                console.log(event.target);
                var parent_box = event.target.parentElement;
                parent_box.remove();
            }, false);
        }
    }
}

optionForBoxDelete();